from shapes import square, triangle, circle
import unittest

class TestShape(unittest.TestCase):
        
    def test_square_area(self):
        sq = square.Square(5)
        self.assertEqual(sq.area(), 25)
    
    def test_square_perimeter(self):
        sq =  square.Square(5)
        self.assertEquals(sq.perimeter(), 20)

    def test_circle_area(self):
        cr = circle.Circle(5)
        self.assertEquals("{:.4f}".format(cr.area()), '78.5398')
        
    def test_circle_perimeter(self):
        cr = circle.Circle(5)
        self.assertEquals("{:.4f}".format(cr.perimeter()), '31.4159')
    
    def test_triangle_area(self):
        tr = triangle.Triangle(5,8)
        self.assertEquals(tr.area(), 20)

    def test_triangle_perimeter(self):
        tr = triangle.Triangle(5,8)
        self.assertEquals(tr.perimeter(), 15)

if __name__ == "__main__":
    unittest.main()
