from shapes import shape as sh

class Circle:

	def __init__ (self,radio):
		self.radio=radio
		self.shape=sh.Shape

	def area(self):
		return self.shape.circle_area(self.radio)

	def perimeter(self):
		return self.shape.circle_perimeter(self.radio)