import math

class Shape:
    
	def square_area(side1):
		return side1*side1

	def square_perimeter(side1):
		return side1 *4

	def triangle_area(base, height):
		return (base*height)/2

	def triangle_perimeter(base):
		return base*3

	def circle_area(radio):
		return math.pi*radio*radio

	def circle_perimeter(radio):
		return 2*(math.pi*radio)
		
	"""
	def area(self,type):
		if type == "circle":
			self.circle_area()
	"""