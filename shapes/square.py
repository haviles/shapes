from shapes import shape as sh

class Square:
    def __init__(self,side):
        self.side=side
        self.shape=sh.Shape
    
    def area(self):
        return self.shape.square_area(self.side)

    def perimeter(self):
        return self.shape.square_perimeter(self.side)