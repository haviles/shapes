from shapes import shape as sh


class Triangle:

    def __init__(self,base,height):
        self.base = base
        self.height = height
        self.shape= sh.Shape

    def area(self):
        return self.shape.triangle_area(self.base,self.height)

    def perimeter(self):
        return self.shape.triangle_perimeter(self.base)